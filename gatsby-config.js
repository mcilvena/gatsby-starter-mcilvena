/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  siteMetadata: {
    title: `gatsby-starter-mcilvena`,
    description: `Troy Mcilvena's Gatsby Starter. TailwindCSS + Netlify`,
  },
  plugins: [
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-mcilvena`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#f9f9f9`,
        theme_color: `#E91E63`,
        display: `minimal-ui`,
        icon: `src/images/icon.png`,
      },
    },
    {
      resolve: `gatsby-plugin-sass`,
      options: {
        postCssPlugins: [require('tailwindcss')],
      },
    },
  ],
};
