import React, { FunctionComponent } from 'react';
import Layout from '../components/layout';
import { useSiteMetadata } from '../hooks/use-site-metadata';

const Home: FunctionComponent = () => {
  const siteMetadata = useSiteMetadata();

  return (
    <Layout>
      <main className="py-24 px-24">
        <h2 className="w-1/2 mb-4">{siteMetadata.description}</h2>
        <div className="w-2/3">
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga
            perspiciatis inventore tenetur, velit voluptate temporibus et illum
            alias assumenda, nihil, maxime accusamus porro sed atque dolorum
            iste dolore ad. Numquam.
          </p>
        </div>
      </main>
    </Layout>
  );
};

export default Home;
