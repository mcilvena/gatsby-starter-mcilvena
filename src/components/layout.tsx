import React, { FunctionComponent } from 'react';
import { Helmet } from 'react-helmet';
import { useSiteMetadata } from '../hooks/use-site-metadata';

import Nav from './nav';

import '../styles/global.scss';

const Layout: FunctionComponent = ({ children }) => {
  const siteMetadata = useSiteMetadata();

  return (
    <>
      <Helmet
        title={siteMetadata.title}
        meta={[
          {
            name: 'description',
            content: siteMetadata.description,
          },
        ]}
      >
        <html lang="en" />
      </Helmet>
      <Nav siteMetadata={siteMetadata} />
      <div className="container mx-auto">{children}</div>
    </>
  );
};

export default Layout;
