import React, { FunctionComponent } from 'react';
import { SiteMetadata } from '../hooks/use-site-metadata';
import { Link } from 'gatsby';

interface NavProps {
  siteMetadata: SiteMetadata;
}

const Nav: FunctionComponent<NavProps> = (props: NavProps) => (
  <nav className="bg-blue-500 text-white">
    <div className="flex flex-wrap container mx-auto items-center">
      <div className="flex">
        <svg
          width="24"
          height="24"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M12 24a12 12 0 100-24 12 12 0 000 24zm-.1-3c4.8 0 9.1-4 9.1-8.7 0-.7-.6-1.3-1.3-1.3-.7 0-1.3.6-1.3 1.3-.1 3.4-3 6.2-6.5 6.2-1.8 0-3.5-.8-4.7-1.9-.2-.2-.5-.4-.9-.4-.7 0-1.3.6-1.3 1.3 0 .4.2.8.5 1A9.3 9.3 0 0012 21z"
            fill="#fff"
          />
        </svg>
        <div className="px-4">
          <span className="font-semibold">{props.siteMetadata.title}</span>
        </div>
      </div>
      <div className="px-4 flex-auto">
        <Link className="inline-block p-4 hover:bg-blue-600" to="/">
          Home
        </Link>
        <Link className="inline-block p-4 hover:bg-blue-600" to="/">
          About
        </Link>
      </div>
      <div className="px-4">
        <Link className="inline-block py-4 px-6 bg-blue-700" to="/">
          Deploy
        </Link>
      </div>
    </div>
  </nav>
);

export default Nav;
