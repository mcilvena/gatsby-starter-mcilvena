import { useStaticQuery, graphql } from 'gatsby';

export interface SiteMetadata {
  description: string;
  title: string;
}

export const useSiteMetadata = (): SiteMetadata => {
  const { site } = useStaticQuery(graphql`
    query SiteMetaDataQuery {
      site {
        siteMetadata {
          description
          title
        }
      }
    }
  `);

  return site.siteMetadata;
};
