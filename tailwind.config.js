module.exports = {
  theme: {
    extend: {
      fontFamily: {
        serif: ['Merriweather', 'serif'],
        sans: ['Merriweather Sans', 'sans-serif'],
      },
    },
  },
};
