# Troy Mcilvena's Gatsby Starter

[View demo](https://gatsby-starter-mcilvena.netlify.app)

> This is a work in progress and subject to significant changes

## Frameworks

- TailwindCSS
- Sass Stylesheets (SCSS)

## Development

- Typescript
- ESLint
- Jest (TODO)
- Prettier

## Deployment

- Netlify (TODO)
